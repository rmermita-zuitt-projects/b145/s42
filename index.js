// alert('Hi');

//A document is your webpage
console.log(document);

//Targeting the first name input field
// document.querySelector('#txt-first-name');
//Alternatively: document.getElementById('txt-first-name'); || document.getElementByTagName('tag-name');

//Contain the query selector code in a constant

// const txtFirstName = document.querySelector("#txt-first-name");

// const spanFirstName = document.querySelector("#span-first-name");

// //Event Listeners

// txtFirstName.addEventListener('keyup', (event) => {
// 	spanFirstName.innerHTML = txtFirstName.value;
// 	console.log(e.target);
// 	console.log(e.target.value);
// });

//Multiple Listeners
	//e shorthand for event

// txtFirstName.addEventListener('keyup', (e) => {
// });

// ACTIVITY
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector('#span-full-name');
txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value + ' ' + txtLastName.value;
	console.log(event.target);
	console.log(event.target.value);
});

txtLastName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value + ' ' + txtLastName.value;
	console.log(event.target);
	console.log(event.target.value);
});



